# ApiDocumentation.IndexEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**indexUsingGET**](IndexEndpointApi.md#indexUsingGET) | **GET** / | index


<a name="indexUsingGET"></a>
# **indexUsingGET**
> &#39;String&#39; indexUsingGET()

index

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.IndexEndpointApi();
apiInstance.indexUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

**&#39;String&#39;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

