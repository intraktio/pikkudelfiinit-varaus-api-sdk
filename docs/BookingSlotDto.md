# ApiDocumentation.BookingSlotDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateEnd** | **Date** |  | [optional] 
**dateStart** | **Date** |  | [optional] 
**maxBookings** | **Number** |  | [optional] 
**productId** | **Number** |  | [optional] 
**timeEnd** | [**Time**](Time.md) |  | [optional] 
**timeStart** | [**Time**](Time.md) |  | [optional] 
**venueId** | **Number** |  | [optional] 
**weekDay** | **Number** |  | [optional] 


