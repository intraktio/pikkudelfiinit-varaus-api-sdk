# ApiDocumentation.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**profileInfo** | [**ProfileInfo**](ProfileInfo.md) |  | [optional] 
**roles** | [**[UserRole]**](UserRole.md) |  | [optional] 


