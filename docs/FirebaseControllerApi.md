# ApiDocumentation.FirebaseControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tokenUsingPOST**](FirebaseControllerApi.md#tokenUsingPOST) | **POST** /auth/firebase/token | token


<a name="tokenUsingPOST"></a>
# **tokenUsingPOST**
> FirebaseToken tokenUsingPOST(opts)

token

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.FirebaseControllerApi();

var opts = { 
  'token': "token_example" // String | token
};
apiInstance.tokenUsingPOST(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| token | [optional] 

### Return type

[**FirebaseToken**](FirebaseToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

