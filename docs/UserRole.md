# ApiDocumentation.UserRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authRole** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 


<a name="AuthRoleEnum"></a>
## Enum: AuthRoleEnum


* `USER` (value: `"ROLE_USER"`)

* `ADMIN` (value: `"ROLE_ADMIN"`)

* `COACH` (value: `"ROLE_COACH"`)




