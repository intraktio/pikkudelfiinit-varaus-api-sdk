# ApiDocumentation.Booking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingSlot** | [**BookingSlot**](BookingSlot.md) |  | [optional] 
**dateEnd** | **Date** |  | [optional] 
**dateStart** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 
**persons** | [**[Person]**](Person.md) |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 


