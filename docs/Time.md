# ApiDocumentation.Time

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_date** | **Number** |  | [optional] 
**day** | **Number** |  | [optional] 
**hours** | **Number** |  | [optional] 
**minutes** | **Number** |  | [optional] 
**month** | **Number** |  | [optional] 
**seconds** | **Number** |  | [optional] 
**time** | **Number** |  | [optional] 
**timezoneOffset** | **Number** |  | [optional] 
**year** | **Number** |  | [optional] 


