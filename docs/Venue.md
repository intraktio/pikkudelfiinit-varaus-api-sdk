# ApiDocumentation.Venue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 


