# ApiDocumentation.CourseEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coursesUsingGET**](CourseEndpointApi.md#coursesUsingGET) | **GET** /courses/ | courses
[**getCourseUsingGET**](CourseEndpointApi.md#getCourseUsingGET) | **GET** /courses/{courseId} | getCourse
[**postCourseUsingPOST**](CourseEndpointApi.md#postCourseUsingPOST) | **POST** /courses/ | postCourse


<a name="coursesUsingGET"></a>
# **coursesUsingGET**
> IterableProduct coursesUsingGET(opts)

courses

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.CourseEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.coursesUsingGET(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**IterableProduct**](IterableProduct.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getCourseUsingGET"></a>
# **getCourseUsingGET**
> Product getCourseUsingGET(courseId)

getCourse

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.CourseEndpointApi();

var courseId = 789; // Number | courseId

apiInstance.getCourseUsingGET(courseId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **courseId** | **Number**| courseId | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="postCourseUsingPOST"></a>
# **postCourseUsingPOST**
> Product postCourseUsingPOST(product)

postCourse

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.CourseEndpointApi();

var product = new ApiDocumentation.Product(); // Product | product

apiInstance.postCourseUsingPOST(product).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**Product**](Product.md)| product | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

