# ApiDocumentation.ProfileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acceptPhoto** | **Boolean** |  | [optional] 
**acceptTerms** | **Boolean** |  | [optional] 
**address** | **String** |  | [optional] 
**billingMethod** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**createdDate** | **Date** |  | [optional] 
**einvoiceInfo** | [**EInvoiceInfo**](EInvoiceInfo.md) |  | [optional] 
**id** | **Number** |  | [optional] 
**imageUrl** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**noEmail** | **Boolean** |  | [optional] 
**noSms** | **Boolean** |  | [optional] 
**tel** | **String** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**zipCode** | **String** |  | [optional] 


<a name="BillingMethodEnum"></a>
## Enum: BillingMethodEnum


* `EBILL` (value: `"EBILL"`)

* `EMAIL` (value: `"EMAIL"`)

* `POSTAL` (value: `"POSTAL"`)




