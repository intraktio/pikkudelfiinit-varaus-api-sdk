# ApiDocumentation.TestEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendEmailUsingGET**](TestEndpointApi.md#sendEmailUsingGET) | **GET** /notifications/sendEmail | sendEmail
[**sendSmsUsingGET**](TestEndpointApi.md#sendSmsUsingGET) | **GET** /notifications/sendSms | sendSms


<a name="sendEmailUsingGET"></a>
# **sendEmailUsingGET**
> NotificationResponse sendEmailUsingGET()

sendEmail

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.TestEndpointApi();
apiInstance.sendEmailUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NotificationResponse**](NotificationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="sendSmsUsingGET"></a>
# **sendSmsUsingGET**
> NotificationResponse sendSmsUsingGET()

sendSms

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.TestEndpointApi();
apiInstance.sendSmsUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NotificationResponse**](NotificationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

