# ApiDocumentation.BookingSlotEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBookingUsingPOST**](BookingSlotEndpointApi.md#addBookingUsingPOST) | **POST** /bookingslots/{bookingSlotId}/book | addBooking
[**getBookingSlotUsingGET**](BookingSlotEndpointApi.md#getBookingSlotUsingGET) | **GET** /bookingslots/{bookingSlotId} | getBookingSlot
[**getBookingSlotsUsingGET**](BookingSlotEndpointApi.md#getBookingSlotsUsingGET) | **GET** /bookingslots/ | Return booking slots
[**postBookingSlotUsingPOST**](BookingSlotEndpointApi.md#postBookingSlotUsingPOST) | **POST** /bookingslots/ | postBookingSlot


<a name="addBookingUsingPOST"></a>
# **addBookingUsingPOST**
> Booking addBookingUsingPOST(info, bookingSlotId)

addBooking

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.BookingSlotEndpointApi();

var info = new ApiDocumentation.Dto(); // Dto | info

var bookingSlotId = 789; // Number | bookingSlotId

apiInstance.addBookingUsingPOST(info, bookingSlotId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **info** | [**Dto**](Dto.md)| info | 
 **bookingSlotId** | **Number**| bookingSlotId | 

### Return type

[**Booking**](Booking.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getBookingSlotUsingGET"></a>
# **getBookingSlotUsingGET**
> BookingSlot getBookingSlotUsingGET(bookingSlotId)

getBookingSlot

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.BookingSlotEndpointApi();

var bookingSlotId = 789; // Number | bookingSlotId

apiInstance.getBookingSlotUsingGET(bookingSlotId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookingSlotId** | **Number**| bookingSlotId | 

### Return type

[**BookingSlot**](BookingSlot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getBookingSlotsUsingGET"></a>
# **getBookingSlotsUsingGET**
> [BookingSlot] getBookingSlotsUsingGET(opts)

Return booking slots

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.BookingSlotEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getBookingSlotsUsingGET(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[BookingSlot]**](BookingSlot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="postBookingSlotUsingPOST"></a>
# **postBookingSlotUsingPOST**
> BookingSlot postBookingSlotUsingPOST(bookingSlotDto)

postBookingSlot

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.BookingSlotEndpointApi();

var bookingSlotDto = new ApiDocumentation.BookingSlotDto(); // BookingSlotDto | bookingSlotDto

apiInstance.postBookingSlotUsingPOST(bookingSlotDto).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookingSlotDto** | [**BookingSlotDto**](BookingSlotDto.md)| bookingSlotDto | 

### Return type

[**BookingSlot**](BookingSlot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

