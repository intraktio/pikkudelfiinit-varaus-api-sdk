# ApiDocumentation.EInvoiceInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  | [optional] 
**operator** | **String** |  | [optional] 


