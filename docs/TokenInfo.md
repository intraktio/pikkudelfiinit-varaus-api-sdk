# ApiDocumentation.TokenInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authId** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**provider** | **String** |  | [optional] 
**verified** | **Boolean** |  | [optional] 


