# ApiDocumentation.Dto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  | [optional] 
**dateOfBirth** | **Date** |  | [optional] 
**name** | **String** |  | [optional] 


