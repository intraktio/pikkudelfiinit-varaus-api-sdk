# ApiDocumentation.Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**price** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 


