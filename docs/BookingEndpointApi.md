# ApiDocumentation.BookingEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserBookingsUsingGET**](BookingEndpointApi.md#getUserBookingsUsingGET) | **GET** /bookings/ | Return user bookings


<a name="getUserBookingsUsingGET"></a>
# **getUserBookingsUsingGET**
> [Booking] getUserBookingsUsingGET()

Return user bookings

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.BookingEndpointApi();
apiInstance.getUserBookingsUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Booking]**](Booking.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

