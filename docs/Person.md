# ApiDocumentation.Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** |  | [optional] 
**dateOfBirth** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 


