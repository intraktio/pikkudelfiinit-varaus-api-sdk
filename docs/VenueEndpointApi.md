# ApiDocumentation.VenueEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getVenuesUsingDELETE**](VenueEndpointApi.md#getVenuesUsingDELETE) | **DELETE** /venues/ | Return venues
[**getVenuesUsingGET**](VenueEndpointApi.md#getVenuesUsingGET) | **GET** /venues/ | Return venues
[**getVenuesUsingHEAD**](VenueEndpointApi.md#getVenuesUsingHEAD) | **HEAD** /venues/ | Return venues
[**getVenuesUsingOPTIONS**](VenueEndpointApi.md#getVenuesUsingOPTIONS) | **OPTIONS** /venues/ | Return venues
[**getVenuesUsingPATCH**](VenueEndpointApi.md#getVenuesUsingPATCH) | **PATCH** /venues/ | Return venues
[**getVenuesUsingPOST**](VenueEndpointApi.md#getVenuesUsingPOST) | **POST** /venues/ | Return venues
[**getVenuesUsingPUT**](VenueEndpointApi.md#getVenuesUsingPUT) | **PUT** /venues/ | Return venues


<a name="getVenuesUsingDELETE"></a>
# **getVenuesUsingDELETE**
> [Venue] getVenuesUsingDELETE(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingDELETE(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingGET"></a>
# **getVenuesUsingGET**
> [Venue] getVenuesUsingGET(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingGET(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingHEAD"></a>
# **getVenuesUsingHEAD**
> [Venue] getVenuesUsingHEAD(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingHEAD(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingOPTIONS"></a>
# **getVenuesUsingOPTIONS**
> [Venue] getVenuesUsingOPTIONS(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingOPTIONS(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingPATCH"></a>
# **getVenuesUsingPATCH**
> [Venue] getVenuesUsingPATCH(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingPATCH(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingPOST"></a>
# **getVenuesUsingPOST**
> [Venue] getVenuesUsingPOST(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingPOST(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getVenuesUsingPUT"></a>
# **getVenuesUsingPUT**
> [Venue] getVenuesUsingPUT(opts)

Return venues

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.VenueEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.getVenuesUsingPUT(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Venue]**](Venue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

