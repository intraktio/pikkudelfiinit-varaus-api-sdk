# ApiDocumentation.BookingSlot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingSlotEnded** | **Boolean** |  | [optional] 
**bookingsSize** | **Number** |  | [optional] 
**dateEnd** | **Date** |  | [optional] 
**dateStart** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 
**maxBookings** | **Number** |  | [optional] 
**product** | [**Product**](Product.md) |  | [optional] 
**timeEnd** | [**Time**](Time.md) |  | [optional] 
**timeStart** | [**Time**](Time.md) |  | [optional] 
**venue** | [**Venue**](Venue.md) |  | [optional] 
**weekDay** | **Number** |  | [optional] 


