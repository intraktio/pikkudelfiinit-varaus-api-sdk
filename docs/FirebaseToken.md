# ApiDocumentation.FirebaseToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**claims** | **Object** |  | [optional] 
**email** | **String** |  | [optional] 
**emailVerified** | **Boolean** |  | [optional] 
**issuer** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**picture** | **String** |  | [optional] 
**uid** | **String** |  | [optional] 


