# ApiDocumentation.UserEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addChildUsingPOST**](UserEndpointApi.md#addChildUsingPOST) | **POST** /users/me/children | addChild
[**getChildUsingGET**](UserEndpointApi.md#getChildUsingGET) | **GET** /users/me/children/{personId} | getChild
[**listChildrenUsingGET**](UserEndpointApi.md#listChildrenUsingGET) | **GET** /users/me/children | Return user&#39;s person entities.
[**listUsingGET**](UserEndpointApi.md#listUsingGET) | **GET** /users/ | list
[**meUsingGET**](UserEndpointApi.md#meUsingGET) | **GET** /users/me | me
[**updateChildUsingPUT**](UserEndpointApi.md#updateChildUsingPUT) | **PUT** /users/me/children/{personId} | updateChild
[**updateMeUsingPUT**](UserEndpointApi.md#updateMeUsingPUT) | **PUT** /users/me | updateMe


<a name="addChildUsingPOST"></a>
# **addChildUsingPOST**
> Person addChildUsingPOST(infant)

addChild

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var infant = new ApiDocumentation.Person(); // Person | infant

apiInstance.addChildUsingPOST(infant).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **infant** | [**Person**](Person.md)| infant | 

### Return type

[**Person**](Person.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getChildUsingGET"></a>
# **getChildUsingGET**
> Person getChildUsingGET(personId)

getChild

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var personId = 789; // Number | personId

apiInstance.getChildUsingGET(personId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personId** | **Number**| personId | 

### Return type

[**Person**](Person.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="listChildrenUsingGET"></a>
# **listChildrenUsingGET**
> [Person] listChildrenUsingGET(opts)

Return user&#39;s person entities.

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.listChildrenUsingGET(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**[Person]**](Person.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="listUsingGET"></a>
# **listUsingGET**
> IterableUser listUsingGET(opts)

list

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var opts = { 
  'filter': ["filter_example"] // [String] | filter
};
apiInstance.listUsingGET(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**[String]**](String.md)| filter | [optional] 

### Return type

[**IterableUser**](IterableUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="meUsingGET"></a>
# **meUsingGET**
> User meUsingGET()

me

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();
apiInstance.meUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateChildUsingPUT"></a>
# **updateChildUsingPUT**
> Person updateChildUsingPUT(infant, personId)

updateChild

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var infant = new ApiDocumentation.Dto(); // Dto | infant

var personId = 789; // Number | personId

apiInstance.updateChildUsingPUT(infant, personId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **infant** | [**Dto**](Dto.md)| infant | 
 **personId** | **Number**| personId | 

### Return type

[**Person**](Person.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="updateMeUsingPUT"></a>
# **updateMeUsingPUT**
> User updateMeUsingPUT(user)

updateMe

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.UserEndpointApi();

var user = new ApiDocumentation.User(); // User | user

apiInstance.updateMeUsingPUT(user).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)| user | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

