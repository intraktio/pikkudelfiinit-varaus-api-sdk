# ApiDocumentation.ProductEndpointApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductsUsingGET**](ProductEndpointApi.md#getProductsUsingGET) | **GET** /products/ | getProducts
[**postProductUsingPOST**](ProductEndpointApi.md#postProductUsingPOST) | **POST** /products/ | postProduct


<a name="getProductsUsingGET"></a>
# **getProductsUsingGET**
> [Product] getProductsUsingGET()

getProducts

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.ProductEndpointApi();
apiInstance.getProductsUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Product]**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="postProductUsingPOST"></a>
# **postProductUsingPOST**
> Product postProductUsingPOST(product)

postProduct

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.ProductEndpointApi();

var product = new ApiDocumentation.Product(); // Product | product

apiInstance.postProductUsingPOST(product).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**Product**](Product.md)| product | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

