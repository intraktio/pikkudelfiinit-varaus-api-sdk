# ApiDocumentation.AuthenticationControllerApi

All URIs are relative to *https://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserUsingGET**](AuthenticationControllerApi.md#getUserUsingGET) | **GET** /auth/user | getUser
[**linkLegacyUsingPOST**](AuthenticationControllerApi.md#linkLegacyUsingPOST) | **POST** /auth/legacy/activate | linkLegacy
[**registerUsingPOST**](AuthenticationControllerApi.md#registerUsingPOST) | **POST** /auth/register | register
[**verifyLegacyUsingPOST**](AuthenticationControllerApi.md#verifyLegacyUsingPOST) | **POST** /auth/legacy/verify | verifyLegacy
[**verifyUsingPOST**](AuthenticationControllerApi.md#verifyUsingPOST) | **POST** /auth/verify | verify


<a name="getUserUsingGET"></a>
# **getUserUsingGET**
> User getUserUsingGET()

getUser

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.AuthenticationControllerApi();
apiInstance.getUserUsingGET().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="linkLegacyUsingPOST"></a>
# **linkLegacyUsingPOST**
> User linkLegacyUsingPOST(info)

linkLegacy

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.AuthenticationControllerApi();

var info = new ApiDocumentation.UsernamePasswordLoginInfo(); // UsernamePasswordLoginInfo | info

apiInstance.linkLegacyUsingPOST(info).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **info** | [**UsernamePasswordLoginInfo**](UsernamePasswordLoginInfo.md)| info | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="registerUsingPOST"></a>
# **registerUsingPOST**
> User registerUsingPOST(info)

register

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.AuthenticationControllerApi();

var info = new ApiDocumentation.RegistrationInfo(); // RegistrationInfo | info

apiInstance.registerUsingPOST(info).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **info** | [**RegistrationInfo**](RegistrationInfo.md)| info | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="verifyLegacyUsingPOST"></a>
# **verifyLegacyUsingPOST**
> User verifyLegacyUsingPOST(info)

verifyLegacy

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.AuthenticationControllerApi();

var info = new ApiDocumentation.UsernamePasswordLoginInfo(); // UsernamePasswordLoginInfo | info

apiInstance.verifyLegacyUsingPOST(info).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **info** | [**UsernamePasswordLoginInfo**](UsernamePasswordLoginInfo.md)| info | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="verifyUsingPOST"></a>
# **verifyUsingPOST**
> TokenInfo verifyUsingPOST(info)

verify

### Example
```javascript
var ApiDocumentation = require('api_documentation');

var apiInstance = new ApiDocumentation.AuthenticationControllerApi();

var info = new ApiDocumentation.RegistrationInfo(); // RegistrationInfo | info

apiInstance.verifyUsingPOST(info).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **info** | [**RegistrationInfo**](RegistrationInfo.md)| info | 

### Return type

[**TokenInfo**](TokenInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

