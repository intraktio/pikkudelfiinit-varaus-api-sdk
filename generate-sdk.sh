#!/bin/bash

#This script automatically generates an sdk from the given api-docs -url

if [ $# -ne "1" ]
then
  echo "Usage: `basename $0` [api-docs-url]"
  exit $E_BADARGSfi
fi

api_docs_url="$1"

java -jar tools/swagger-codegen-cli.jar generate -i $api_docs_url -l javascript -DusePromises=true -DuseES6

function search_replace() {
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    # Linux
    sed -i "$@"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    sed -i '' -e "$@"
  fi
}

search_replace 's/"version": "\([0-9]\.*\)*"/"version": "1.0.0"/' package.json

for file in $(find . ! -path "./node_modules/*" -name "*.js"); do
  # Fix amd type imports, since it breaks our React app
  search_replace "s/if (typeof define === 'function' && define.amd)/if (false)/" $file
  # Fix invalid date parsing
  search_replace "s/new Date(str\.replace/new Date(!isNaN(str) ? +str : str.replace/" $file
  # Handle Time objects as strings
  search_replace "s/Time\.constructFromObject(\(.*\))/ApiClient.convertToType(\1, 'String')/" $file
done
