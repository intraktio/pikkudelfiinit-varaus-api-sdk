/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (false) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ApiDocumentation);
  }
}(this, function(expect, ApiDocumentation) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ApiDocumentation.ProfileInfo();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ProfileInfo', function() {
    it('should create an instance of ProfileInfo', function() {
      // uncomment below and update the code to test ProfileInfo
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be.a(ApiDocumentation.ProfileInfo);
    });

    it('should have the property acceptPhoto (base name: "acceptPhoto")', function() {
      // uncomment below and update the code to test the property acceptPhoto
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property acceptTerms (base name: "acceptTerms")', function() {
      // uncomment below and update the code to test the property acceptTerms
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property address (base name: "address")', function() {
      // uncomment below and update the code to test the property address
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property city (base name: "city")', function() {
      // uncomment below and update the code to test the property city
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property createdDate (base name: "createdDate")', function() {
      // uncomment below and update the code to test the property createdDate
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property name (base name: "name")', function() {
      // uncomment below and update the code to test the property name
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property noEmail (base name: "noEmail")', function() {
      // uncomment below and update the code to test the property noEmail
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property noSms (base name: "noSms")', function() {
      // uncomment below and update the code to test the property noSms
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

    it('should have the property zipCode (base name: "zipCode")', function() {
      // uncomment below and update the code to test the property zipCode
      //var instane = new ApiDocumentation.ProfileInfo();
      //expect(instance).to.be();
    });

  });

}));
