/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (false) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ApiDocumentation);
  }
}(this, function(expect, ApiDocumentation) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ApiDocumentation.TestEndpointApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TestEndpointApi', function() {
    describe('testUsingDELETE', function() {
      it('should call testUsingDELETE successfully', function(done) {
        //uncomment below and update the code to test testUsingDELETE
        //instance.testUsingDELETE(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingGET', function() {
      it('should call testUsingGET successfully', function(done) {
        //uncomment below and update the code to test testUsingGET
        //instance.testUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingHEAD', function() {
      it('should call testUsingHEAD successfully', function(done) {
        //uncomment below and update the code to test testUsingHEAD
        //instance.testUsingHEAD(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingOPTIONS', function() {
      it('should call testUsingOPTIONS successfully', function(done) {
        //uncomment below and update the code to test testUsingOPTIONS
        //instance.testUsingOPTIONS(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingPATCH', function() {
      it('should call testUsingPATCH successfully', function(done) {
        //uncomment below and update the code to test testUsingPATCH
        //instance.testUsingPATCH(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingPOST', function() {
      it('should call testUsingPOST successfully', function(done) {
        //uncomment below and update the code to test testUsingPOST
        //instance.testUsingPOST(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('testUsingPUT', function() {
      it('should call testUsingPUT successfully', function(done) {
        //uncomment below and update the code to test testUsingPUT
        //instance.testUsingPUT(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingDELETE', function() {
      it('should call usersUsingDELETE successfully', function(done) {
        //uncomment below and update the code to test usersUsingDELETE
        //instance.usersUsingDELETE(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingGET', function() {
      it('should call usersUsingGET successfully', function(done) {
        //uncomment below and update the code to test usersUsingGET
        //instance.usersUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingHEAD', function() {
      it('should call usersUsingHEAD successfully', function(done) {
        //uncomment below and update the code to test usersUsingHEAD
        //instance.usersUsingHEAD(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingOPTIONS', function() {
      it('should call usersUsingOPTIONS successfully', function(done) {
        //uncomment below and update the code to test usersUsingOPTIONS
        //instance.usersUsingOPTIONS(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingPATCH', function() {
      it('should call usersUsingPATCH successfully', function(done) {
        //uncomment below and update the code to test usersUsingPATCH
        //instance.usersUsingPATCH(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingPOST', function() {
      it('should call usersUsingPOST successfully', function(done) {
        //uncomment below and update the code to test usersUsingPOST
        //instance.usersUsingPOST(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('usersUsingPUT', function() {
      it('should call usersUsingPUT successfully', function(done) {
        //uncomment below and update the code to test usersUsingPUT
        //instance.usersUsingPUT(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
