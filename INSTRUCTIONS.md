# Instructions for generating the SDK

## Generating the JavaScript SDK

1. Start the pikkudelfiinit-varaus-api backend server, this should host the Swagger JSON file at some URL, e.g. http://localhost:8080/v2/api-docs
2. Install [swagger-codegen](https://swagger.io/swagger-codegen/)
3. Generate the SDK for JavaScript with the additional options for ES6 and Promises by using the following command, assuming that the previous URL for the JSON-file is correct.
	
	```swagger-codegen generate -i http://localhost:8080/v2/api-docs -l javascript -DusePromises=true -DuseES6```
4. Check the docs for how to use the SDK in the README.md-file.  
 
 ## Modifying the SDK
- https://github.com/facebook/create-react-app/issues/3247 , a dirty fix for this is to go through every file and remove the first `if (...)` -clause from the import. 
- You can change the basepath url in the ApiClient.js-file. 
- If `npm install` fails try changing the version number to 1.0.0 instead of 1 in the package.json-file. 